﻿using System;

namespace Hello_world
{
    class Program
    {
        
        private static int Age; //integer 
        private static string message =  " My age is "; //string
        private static float decimal_num = 16.5f; //Floating point number
        private static int[] myNum = { 1, 2, 3, 4, 5, 6, 7 }; //an array
        private static char myLetter = 'k'; //Character
        private bool myBool = true;//Boolean
        private static long longNum = 10000000000L;
        private static double d1 = 12E4D;

        static void Main(string[] args)
        {
            
            //Delcares age variable.
            Age = 16;

            //Prints age and message
            Console.WriteLine(message + Age);

            //Prints decimal number
            Console.WriteLine(decimal_num);

            //Prints long number
            Console.WriteLine(longNum);

            //Prints Char variable
            Console.WriteLine(myLetter);

            //Prints double variable
            Console.WriteLine(d1);


            //For as many items in the array, it will print the next item
            foreach (var item in myNum)
            {
                //prints the array
                Console.WriteLine(item);

            }



        }

    }
}
